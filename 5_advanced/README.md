# 编程高级技巧

通过前面的学习与练习，能够编写一些简单的程序，但是编写大型程序除了算法、编程基础之外，还需要项目、库组织、编程规范、设计模式等技巧。这些技巧不仅仅能提高效率，而且是完善计算机思维的一个重要的环节。

## 1. [编程规范](programming_standard)

对于不同的编程语言来说，具体的编码规范可以有很大的不同，但是其宗旨都是一致的，就是保证代码在高质量完成需求的同时具备良好的可读性、可维护性。例如我们可以规定某个项目的C语言程序要遵循这样的规定：变量的命名，头文件的书写和`#include` 等等。

编程规范只能在多写程序的基础上，通过不断的反思、总结才能形成。另外可以参考一个比较好的开源程序，学习它的代码规范。最基本的原则包括：

1. 命名规范
2. 文件排版，格式等
3. 注释
4. 函数、类的使用
5. 错误处理

Google的编程规范大家可以作为主要的参考：
![code standard](programming_standard/Google_Cpp_Style_guide_CN.png)

更多参考：
* https://zhuanlan.zhihu.com/p/71782780
* [Google C++ Style Guide （中文）](programming_standard/Google_Cpp_Style_guide_CN.pdf)



## 2. [设计模式](4_DesignPattern_UML)

设计模式（Design Pattern）代表了最佳的实践，在面向对象的编程中被很多老鸟们反复使用。使用设计模式有很多好处：

- 可重用代码
- 保证代码可靠性
- 使代码更易被他人理解
- …

毫无疑问，设计模式于己、于人、于系统都是多赢的。《设计模式》之于程序员，就好比《圣经》之于耶稣信徒一样，意义可想而知！大家在编程练习过程需要多思考自己写的代码如何用更好的代码组织方式，能否用更清晰易懂的模式来实现，这样不仅方便自己理解，也让其他人更好理解，从而带来更好的协同开发。



更多的资料可以参考：

* https://blog.csdn.net/u011012932/article/details/66974516

* https://github.com/FengJungle/DesignPattern

* https://gitee.com/pi-lab/code_cook/tree/master/c++/DesignPattern



## 3. 开发模型

* [开发模型的演化](https://www.toutiao.com/i6863271015640728068/)
