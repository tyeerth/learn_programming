# 编码规范

### Python 编程规范

[编程规范文档](https://gitee.com/intelligent-system-group/learn_programming/blob/master/5_advanced/programming_standard/Google_Python_Style_Guide.pdf)

### C++ 编程规范

[编程规范文档](https://gitee.com/intelligent-system-group/learn_programming/blob/master/5_advanced/programming_standard/Google_Cpp_Style_guide_CN.pdf)

![Google_Cpp_Style_guide_CN.png](Google_Cpp_Style_guide_CN.png)