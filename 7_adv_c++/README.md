# Stage 7 - 现代C++

主要学习C++ 11， C++14， C++20等最新功能、函数。


## 学习的思路
1. 学习C++11的基本用法
2. 使用[Svar](https://gitee.com/pi-lab/Svar)，并琢磨其中所使用的高级C++技巧
3. 学习G2O，Ceres等库中高级的C++技巧



## References

* [《现代C++教程（中文版）》.pdf](books/《现代C++教程（中文版）》.pdf)

* [《现代C++教程（英文版）》.pdf](books/《现代C++教程（英文版）》.pdf)

