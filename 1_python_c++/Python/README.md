# Ptthon基础

## 1. 学习内容
学习Python的基本语法，练习编写基本的程序，包括：
* 基本的用法，环境搭建，运算符，循环、判断、打印
* 常见数据类型：字符串、列表、元祖、字典
* 函数的定义、调用
* 类，面向对象编程
* 文件、IO操作
* Numpy
* 如何使用IDE环境编写、调试程序；或者在Linux下用GCC编译器编译程序

## 2. 要求
请将练习的程序按照类别保存到对应的目录里。


## 3. 学习资料
- 资料（可以使用在线的教程，或者自己找比较合适的）：
    - [Python3 教程](https://www.runoob.com/python3/python3-tutorial.html) ，通过这个教程快速学会Python的语法等。
    - 如果对自己有更高的要求，可以学习[Python3 高级教程](https://www.runoob.com/python3/python3-tutorial.html)。不过最好还是用上面普通的教程先把基础知识学好之后，再学习Python更有效率。
    - Numpy是 Python 语言的一个扩展程序库，支持大量的维度数组与矩阵运算[Numpy 教程](https://www.runoob.com/numpy/numpy-tutorial.html)。在机器学习和深度学习中都是特别实用的工具。
  

## 4. 小技巧
### 4.1 如何在一个程序里，写多个测试、验证代码
具体可以参考[多个测试函数例子](1_basicusage/MultiTestFuctions.cpp)，里面的用法。这样让程序保持清晰、易懂。




## Reference
* https://www.w3resource.com/python/python-tutorial.php
* https://edabit.com/challenges/python3