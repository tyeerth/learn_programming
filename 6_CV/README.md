# Stage 6 - CV(计算机视觉)

OpenCV是一个基于Apache2.0许可（开源）发行的跨平台计算机视觉和机器学习软件库。
OpenCV用C++语言编写，它具有C ++，Python，Java和MATLAB接口。


## 1.OpenCV简介
在学习 OpenCV之前，需要具备基础的图像处理的知识，这里选择《图像处理入门100题，python和c++版本都有》这个资料来学习计算机视觉，OpenCV提供的视觉处理算法非常丰富，并且它部分以C语言编写，加上其开源的特性，处理得当，不需要添加新的外部支持也可以完整的编译链接生成执行程序，所以很多人用它来做算法的移植。


## 2.学习路线

这里给出了很多OpenCV的例子，在[OpenCV100问](6_adv_projects/ImageProcessing100Wen)中。
作业要求是请选择20个问题进行程序编写练习,并存储至[practice](6_adv_projects/practice)文件夹中。

请注意，不能直接抄袭源代码。